function processResponse(data) {
    $('#result').html(typeof data == 'string' ? data : 'error');
}

function processError(data) {
    $('#result').html('Ошибка: ' + data.statusText + '. См. подробности в JS-консоли');
    console.log(data.responseText);
}

function evaluate(){
    let formData = new FormData();
    formData.append('operand1',  $('#operand1').val());
    formData.append('operand2',  $('#operand2').val());
    formData.append('operation', $('#operation').val());
    formData.append('_token',    $('input[name="_token"]').val());

    $('#result').html('...');

    $.ajax({
        url: '/evaluate',
        type: 'post',
        data: formData,
        dataType: 'json',
        processData: false,
        contentType: false,
        success: function(data) {
            processResponse(data);
        },
        error: function(data) {
            processError(data);
        }
    });
}


$(document).ready(function() {

    $('#evaluate').on('click', function () {
        evaluate();
    });

});
