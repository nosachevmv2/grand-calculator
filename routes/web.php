<?php

use App\Http\Controllers\AdminController;
use App\Http\Controllers\CalculatorController;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/',          [CalculatorController::class, 'index']);
Route::post('/evaluate', [CalculatorController::class, 'evaluate']);

Route::prefix('admin')
    ->middleware('auth')
    ->group(function () {

        Route::get('/',            [AdminController::class, 'index'])->name('admin');

        Route::get('/operations',  [AdminController::class, 'showOperations'])->name('admin.operations.show');
        Route::post('/operations', [AdminController::class, 'updateOperations'])->name('admin.operations.update');

    });


Route::get('/dashboard', function () {
    return redirect('/');
})->middleware(['auth'])->name('dashboard');

require __DIR__.'/auth.php';
