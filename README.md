<p align="center">
    <a href="https://laravel.com" target="_blank">
        <img src="https://raw.githubusercontent.com/laravel/art/master/logo-lockup/5%20SVG/2%20CMYK/1%20Full%20Color/laravel-logolockup-cmyk-red.svg" width="200">
    </a>
</p>

## Grand calculator

#####Калькулятор для больших чисел (демо-проект)

- Использует PHP-расширение GMP
- Работает с дробными аргументами
- Сложение, вычитание, умножение
- Настройка доступных операций
- Авторизованный админский вход

### Установка

`composer install`

`php artisan migrate`

`php artisan db:seed`

### Авторизация

- логин: admin@calc.ru
- пароль: password

### Список файлов

Код калькулятора залит в `develop`

В ветке `master` - пустой Laravel и авторизация

![files list](public\git_diff_develop.png)

### License

The Laravel framework is open-sourced software licensed under the [MIT license](https://opensource.org/licenses/MIT).
