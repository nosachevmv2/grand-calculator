@extends('layouts.main')

@section('title')
    @lang('Grand calculator')
@endsection

<script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/3.5.1/jquery.min.js" crossorigin="anonymous"
        integrity="sha512-bLT0Qm9VnAYZDflyKcBaQ2gg0hSYNQrJ8RilYldYQ1FxQYoCLtUjuuRuZo+fjqhx/qtq/1itJ0C2ejDxltZVFg==">
</script>
<script src="/assets/calc.js" async></script>
<link  href="/assets/calc.css" rel="stylesheet">

@section('content')
    <form action="" method="post" enctype="multipart/form-data">
        {{ csrf_field() }}
        <textarea name="operand1" id="operand1" cols="50" rows="3"></textarea>
        <select name="operation" id="operation">
            @foreach($operations as $code => $name)
                <option value="<?=$code?>"><?=$name?></option>
            @endforeach
        </select>
        <textarea name="operand2" id="operand2" cols="50" rows="3"></textarea>

        <div class="button" id="evaluate">вычислить</div>

        <textarea disabled id="result" cols="50" rows="7"></textarea>
    </form>
@endsection
