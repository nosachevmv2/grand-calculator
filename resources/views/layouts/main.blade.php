<!DOCTYPE html>
<!-- Initial template was created By CodingNepal www.codingnepalweb.com -->
<!-- https://www.codinglabweb.com/2020/12/responsive-neumorphism-navigation-bar.html -->
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta name="csrf-token" content="{{ csrf_token() }}">

    <title> Grand calculator </title>

    <link rel="stylesheet" href="/assets/main.css">
    <link rel="stylesheet" href="{{ asset('css/app.css') }}">
    <link rel="stylesheet" href="https://fonts.googleapis.com/css2?family=Nunito:wght@400;600;700&display=swap">
    <script src="https://kit.fontawesome.com/a076d05399.js"></script>
    <script src="{{ asset('js/app.js') }}" defer></script>
</head>
<body>
    <nav>
        <div class="menu">
            <input type="checkbox" id="check">
            <div class="logo">Grand calculator</div>
            <ul>
                <li><label class="btn cancel" for="check"><i class="fas fa-times"></i></label></li>
                <li><a href="{{ url('/')  }}">Калькулятор</a></li>
                <li><a href="{{ route('admin.operations.show') }}">Настройки</a></li>
                <li>
                    @if (Route::has('login'))
                        @auth
                            <form method="POST" action="{{ route('logout') }}">
                                @csrf
                                <button type="submit" id="logout">logout</button>
                            </form>
                        @else
                            <a href="{{ route('login') }}">login</a>
                        @endauth
                    @endif
                </li>
            </ul>
            <label for="check" class="btn bars"><i class="fas fa-bars"></i></label>
        </div>
    </nav>
    <main>
        <div class="content">
            @yield('content')
        </div>
        <div class="messages">
            @if (Session::has('flash_message'))
                <h4>{{ Session::get('flash_message') }}</h4>
            @endif
        </div>
    </main>
</body>
</html>
