@extends('layouts.main')

@section('title')
    @lang('Admin panel')
@endsection

<style>
    input{
        margin: 12px;
    }
</style>

@section('content')
    <form action="{{ route('admin.operations.update') }}" method="post" enctype="multipart/form-data">
        {{ csrf_field() }}
        @foreach($operations as $name => $available)
            <input type="checkbox" name="operations[]" value="<?=$name?>" id="operation_<?=$name?>"
                   <?=($available ? 'checked' : '')?> class="" />
            <label for="operation_<?=$name?>" class=""><?=$name?></label><br>
        @endforeach

        <input type="submit" class="button" value="сохранить" />
    </form>

@endsection
