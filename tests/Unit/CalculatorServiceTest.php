<?php

namespace Tests\Unit;

use App\Services\CalculatorService;
use PHPUnit\Framework\TestCase;

class CalculatorServiceTest extends TestCase
{
    private CalculatorService $service;

    public function setUp() : void
    {
        $this->service = resolve(CalculatorService::class);


    }

    /**
     * @test
     *
     * @dataProvider calculateTestDataProvider()
     *
     * @param string $operation
     * @param string $operand1
     * @param string $operand2
     * @param string $reference
     *
     * @return void
     */
    public function calculateTest(string $operation, string $operand1, string $operand2, string $reference)
    {
        $result = $this->service->calculate(
            ['operation' => $operation, 'operand1' => $operand1, 'operand2' => $operand2]
        );

        $this->assertEquals($reference, $result);
    }

    /**
     * @return array
     */
    public function calculateTestDataProvider()
    {
        return [
            __LINE__ => ['operation' => 'add',  'operand1' => '500', 'operand2' => '300', 'reference' => '800'],
            __LINE__ => ['operation' => 'sub',  'operand1' => '500', 'operand2' => '300', 'reference' => '200'],
            __LINE__ => ['operation' => 'sub',  'operand1' => '3.5', 'operand2' => '4',   'reference' => '-0.5'],
            __LINE__ => ['operation' => 'sub',  'operand1' => '3.5', 'operand2' => '-4',  'reference' => '7.5'],
            __LINE__ => ['operation' => 'mult', 'operand1' => '1.3', 'operand2' => '-5',  'reference' => '-6.5'],

        ];
    }

}
