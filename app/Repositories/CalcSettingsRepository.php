<?php

namespace App\Repositories;

use App\Models\CalcSettings;

class CalcSettingsRepository
{
    /**
     * @param string $name
     *
     * @return CalcSettings|null
     */
    public function get(string $name) : ?CalcSettings
    {
        return CalcSettings::where('param_name', $name)->first();
    }

    /**
     * @param string $name
     * @param array $value
     *
     * @return bool
     */
    public function set(string $name, array $value) : bool
    {
        $settings = CalcSettings::where('param_name', $name)->first()
             ?? new CalcSettings();

        $settings->setName($name);
        $settings->setValue($value);

        return $settings->save();
    }

}
