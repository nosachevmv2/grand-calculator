<?php

namespace App\Services\Base;

interface ICalcOperationMult
{
    /**
     * @param string $first
     * @param string $second
     *
     * @return string
     */
    public static function getResultMult(string $first, string $second) : string;
}
