<?php

namespace App\Services\Base;

interface ICalcOperationAddSub
{
    /**
     * @param string $first
     * @param string $second
     *
     * @return string
     */
    public static function getResultAdd(string $first, string $second) : string;

    /**
     * @param string $first
     * @param string $second
     *
     * @return string
     */
    public static function getResultSub(string $first, string $second) : string;
}
