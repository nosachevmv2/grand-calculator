<?php

namespace App\Services;

use App\Services\Base\ICalcOperationAddSub;
use App\Services\Base\ICalcOperationMult;

class CalculatorService
{
    private CalcSettingsService  $settingsService;
    private ICalcOperationAddSub $operationAddSub;
    private ICalcOperationMult   $operationMult;

    public function __construct(
        CalcSettingsService  $settingsService,
        ICalcOperationAddSub $operationAddSub,
        ICalcOperationMult   $operationMult
    )
    {
        $this->settingsService = $settingsService;
        $this->operationAddSub = $operationAddSub;
        $this->operationMult   = $operationMult;
    }

    /**
     * Обработка задания в сборе
     *
     * @param array $requestData
     *
     * @return string
     */
    public function calculate(array $requestData) : string
    {
        $operation = $requestData['operation'];
        $availableOperations = $this->getAvailableOperations();

        if(!array_key_exists($operation, $availableOperations)) {
            return "Операция `$operation` отключена в настройках";
        }

        if(!method_exists($this, $operation)) {
            return "Операция `$operation` пока не поддерживается";
        }

        return $this->{$operation}($requestData);
    }

    /**
     * Список доступных операций
     *
     * @return array  [operation1_code => name, ... ]
     */
    public function getAvailableOperations() : array
    {
        return $this->settingsService->getAvailableOperations();
    }


    /**
     * @param array $operands
     *
     * @return string
     */
    private function add(array $operands) : string
    {
        return $this->operationAddSub->getResultAdd($operands['operand1'], $operands['operand2']);
    }

    /**
     * @param array $operands
     *
     * @return string
     */
    private function sub(array $operands) : string
    {
        return $this->operationAddSub->getResultSub($operands['operand1'], $operands['operand2']);
    }

    /**
     * @param array $operands
     *
     * @return string
     */
    private function mult(array $operands) : string
    {
        return $this->operationMult->getResultMult($operands['operand1'], $operands['operand2']);
    }

}
