<?php

namespace App\Services;

use App\Services\Base\ICalcOperationAddSub;

class CalcOperationAddSubGMP implements ICalcOperationAddSub
{
    /**
     * @inheritDoc
     */
    public static function getResultAdd(string $first, string $second) : string
    {
        [$first, $second, $fracPartLength] = self::shiftAwayFractionParts($first, $second);

        $solidResult = gmp_add($first, $second);

        return self::restoreFractionPart($solidResult, $fracPartLength);
    }

    /**
     * @inheritDoc
     */
    public static function getResultSub(string $first, string $second) : string
    {
        [$first, $second, $fracPartLength] = self::shiftAwayFractionParts($first, $second);

        $solidResult = gmp_sub($first, $second);

        return self::restoreFractionPart($solidResult, $fracPartLength);
    }


    /**
     * Подготовка данных для сдачи функциям GMP.
     * GMP работает только с целыми числами -
     * дробные нужно предварительно допилить.
     *
     * Выравниванием ширину дробных частей,
     * запоминаем её для будущей распаковки
     * и удаляем десятичную точку из обеих строк.
     *
     * @param string $first
     * @param string $second
     *
     * @return array [modifiedFirst, modifiedSecond, fracPartLength]
     */
    private static function shiftAwayFractionParts(string $first, string $second) : array
    {
        $splitFirst  = explode('.', $first);
        $splitSecond = explode('.', $second);

        [$intFirst,  $fracFirst ] = [$splitFirst[0],  $splitFirst[1]  ?? '0'];
        [$intSecond, $fracSecond] = [$splitSecond[0], $splitSecond[1] ?? '0'];

        $fracLength = max(
            strlen($fracFirst),
            strlen($fracSecond)
        );

        return [
            $intFirst  . str_pad($fracFirst,  $fracLength, '0', STR_PAD_RIGHT),
            $intSecond . str_pad($fracSecond, $fracLength, '0', STR_PAD_RIGHT),
            $fracLength
        ];
    }

    /**
     * Восстанавливаем закатанную ранее дробную часть.
     *
     * @param string $solidNumber
     * @param int    $fracPartLength
     *
     * @return string
     */
    private static function restoreFractionPart(string $solidNumber, int $fracPartLength) : string
    {
        $sign = ($solidNumber[0] === '-' ? '-': '');
        $unsigned = str_replace('-', '', $solidNumber);
        $unsigned = str_pad($unsigned, $fracPartLength, '0', STR_PAD_LEFT);

        $int  = substr($unsigned, 0, strlen($unsigned) - $fracPartLength);
        $frac = substr($unsigned, -$fracPartLength);
        $frac = rtrim($frac, "0");

        return $sign . ($int ?: '0') . ($frac ? '.' . $frac : '');
    }

}
