<?php

namespace App\Services;

use App\Services\Base\ICalcOperationAddSub;

class CalcOperationAddSubCore implements ICalcOperationAddSub
{
    /**
     * @inheritDoc
     */
    public static function getResultAdd(string $first, string $second) : string
    {
        $result = (int)$first + (int)$second;

        return (string)$result;
    }

    /**
     * @inheritDoc
     */
    public static function getResultSub(string $first, string $second) : string
    {
        $result = (int)$first - (int)$second;

        return (string)$result;
    }

}
