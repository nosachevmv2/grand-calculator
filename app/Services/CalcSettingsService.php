<?php

namespace App\Services;

use App\Repositories\CalcSettingsRepository;

class CalcSettingsService
{
    private CalcSettingsRepository $settingsRepo;

    public function __construct(
        CalcSettingsRepository $settingsRepo
    )
    {
        $this->settingsRepo = $settingsRepo;
    }

    /**
     * Получение списка доступных операций с названиями
     *
     * @return array  [operation1_code => name, ... ]
     */
    public function getAvailableOperations() : array
    {
        $setting = $this->settingsRepo->get('operations');
        $availableList = optional($setting)->getValue() ?? [];

        $operationsSupported = config('calculator.operations', []);

        return array_filter(
            $operationsSupported,
            fn($code) => in_array($code, $availableList),
            ARRAY_FILTER_USE_KEY
        );
    }

    /**
     * Установка списка доступных операций
     *
     * @param array $operations  [operation1_code, ... ]  индексный
     * @return bool
     */
    public function setAvailableOperations(array $operations) : bool
    {
        $operationsSupported = config('calculator.operations', []);
        $availableList = array_filter(
            $operations,
            fn($code) => array_key_exists($code, $operationsSupported)
        );

        return $result = $this->settingsRepo->set('operations', $availableList);
    }

    /**
     * Список всех операций со статусом доступности каждой
     *
     * @return array  [operation1_code => boolean, ... ]
     */
    public function getAllOperationsWithAvailability() : array
    {
        $operationsSupported = config('calculator.operations', []);
        $operationsAvailable = $this->getAvailableOperations();

        return array_map(
            fn($name) => in_array($name, $operationsAvailable),
            $operationsSupported
        );
    }

}
