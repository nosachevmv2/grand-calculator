<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class CalculatorForm extends FormRequest
{
    /**
     * Пускаем без авторизации
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Правила валидации
     *
     * @return array
     */
    public function rules()
    {
        return [
            'operand1'  => 'required|regex:/^-?\d+\.?\d*$/',
            'operand2'  => 'required|regex:/^-?\d+\.?\d*$/',
            'operation' => 'required|alpha_num',
        ];
    }
}
