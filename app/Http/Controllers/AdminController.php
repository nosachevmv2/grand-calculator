<?php

namespace App\Http\Controllers;

use App\Services\CalcSettingsService;
use Illuminate\Contracts\View\View;
use Illuminate\Http\RedirectResponse;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Session;

class AdminController extends Controller
{
    private CalcSettingsService $settingsService;

    public function __construct(CalcSettingsService $settingsService)
    {
        $this->settingsService = $settingsService;
    }

    /**
     * @return View|RedirectResponse
     */
    public function index()
    {
        return redirect('admin/operations');
    }


    /**
     * @return View|RedirectResponse
     */
    public function showOperations()
    {
        $operations = $this->settingsService->getAllOperationsWithAvailability();

        return view('admin_operations', [
            'operations' => $operations
        ]);
    }

    /**
     * @param Request $request
     *
     * @return View|RedirectResponse
     */
    public function updateOperations(Request $request)
    {
        $operations = $request->get('operations');

        $success = $this->settingsService->setAvailableOperations($operations);
        Session::flash('flash_message', $success ? 'Успешно!' : 'Ошибка сохранения!');

        return redirect('admin/operations');
    }
}
