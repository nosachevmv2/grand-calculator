<?php

namespace App\Http\Controllers;

use App\Http\Requests\CalculatorForm;
use App\Services\CalculatorService;
use Illuminate\Contracts\View\View;
use Illuminate\Http\JsonResponse;

class CalculatorController extends Controller
{
    private CalculatorService $calcService;

    public function __construct(CalculatorService $calcService)
    {
        $this->calcService = $calcService;
    }

    /**
     * @return View
     */
    public function index()
    {
        $availableOperations = $this->calcService->getAvailableOperations();

        return view('calculator', [
            'operations' => $availableOperations
        ]);
    }

    /**
    * @param  CalculatorForm $request
    * @return JsonResponse
    */
    public function evaluate(CalculatorForm $request)
    {
        $result = $this->calcService->calculate(
            $request->all()
        );

        return response()->json($result, 200);
    }
}
