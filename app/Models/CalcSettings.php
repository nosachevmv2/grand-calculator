<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class CalcSettings extends Model
{
    use HasFactory;

    protected $table = 'calc_settings';

    protected $casts = [
        'param_value' => 'array',
    ];

    /**
     * @return string
     */
    public function getName() : string
    {
        return $this->param_name;
    }

    /**
     * @param string $name
     */
    public function setName(string $name) : void
    {
        $this->param_name = $name;
    }

    /**
     * @return array
     */
    public function getValue() : array
    {
        return $this->param_value;
    }

    /**
     * @param array $value
     */
    public function setValue(array $value) : void
    {
        $this->param_value = $value;
    }

}
