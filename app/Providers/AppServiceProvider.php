<?php

namespace App\Providers;

use App\Services\Base\ICalcOperationAddSub;
use App\Services\Base\ICalcOperationMult;
use App\Services\CalcOperationAddSubCore;
use App\Services\CalcOperationAddSubGMP;
use App\Services\CalcOperationMultGMP;
use Illuminate\Support\ServiceProvider;

class AppServiceProvider extends ServiceProvider
{
    private array $bindDI = [
        ICalcOperationAddSub::class => CalcOperationAddSubGMP::class,
        ICalcOperationMult::class   => CalcOperationMultGMP::class,

    ];

    /**
     * Register any application services.
     *
     * @return void
     */
    public function register()
    {
        //
    }

    /**
     * Bootstrap any application services.
     *
     * @return void
     */
    public function boot()
    {
        foreach ($this->bindDI as $interface => $implementation) {
            $this->app->bind($interface, $implementation);
        }
    }
}
