<?php

namespace Database\Seeders;

use App\Models\User;
use Illuminate\Database\Seeder;

class UserSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        User::factory()->create([
            'name'     => 'admin',
            'email'    => 'admin@calc.ru',
            'password' => '$2y$10$9mzbJYxvbDg.DXxmNb4wLefKCiscK6GFdJEByB7xVG/jUgPqPwQcC', // 'password'
        ]);
    }
}
