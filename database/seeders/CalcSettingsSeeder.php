<?php

namespace Database\Seeders;

use App\Models\CalcSettings;
use Illuminate\Database\Seeder;

class CalcSettingsSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        CalcSettings::factory()->create([
            'param_name'  => 'operations',
            'param_value' => array_keys(config('calculator.operations', [])),
        ]);
    }
}
