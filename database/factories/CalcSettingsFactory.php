<?php

namespace Database\Factories;

use App\Models\CalcSettings;
use Illuminate\Database\Eloquent\Factories\Factory;

class CalcSettingsFactory extends Factory
{
    /**
     * The name of the factory's corresponding model.
     *
     * @var string
     */
    protected $model = CalcSettings::class;

    /**
     * Define the model's default state.
     *
     * @return array
     */
    public function definition()
    {
        return [
            'param_name'  => $this->faker->title,
            'param_value' => [
                $this->faker->randomLetter,
            ],
        ];
    }

}
